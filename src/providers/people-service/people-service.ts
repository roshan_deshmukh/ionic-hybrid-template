import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the PeopleServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PeopleServiceProvider {
  private data: Map<string, any[]> = new Map<string, any[]>();
  constructor(public http: HttpClient) {
    console.log('Hello PeopleServiceProvider Provider');
  }

  public async getUsers() {
    try {
      // making API call which fetch 10 random users from back end
      const res = await this.http.get('https://randomuser.me/api/?results=10').toPromise();

      return res;
    } catch (error) {
      console.log(error);
    }
  }
}
