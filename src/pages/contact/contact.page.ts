import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { PeopleServiceProvider } from '../../providers/people-service/people-service';

@Component({
  selector: 'ib-page-contact',
  templateUrl: 'contact.page.html'
})
export class ContactPage {
  public users: any;
  constructor(public navCtrl: NavController, public peopleService: PeopleServiceProvider) {

  }
  public ionViewWillEnter() {
    // invoking service method to fetch user
    this.users = this.peopleService.getUsers();
  }
}
